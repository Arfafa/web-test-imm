<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        $data_days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        $date_now = date("l");
    ?>
    <div class="container">
        <div class="sub-container">
            <p> Today is <span class="today"><?php echo "$date_now"?></span></p>
            <div class="days">
                <?php
                    $i = 0;
                    foreach ($data_days as $day) {
                        if ($date_now === $day) {
                            echo "<div class='sub-day' style='background-color: green;'>$day</div>";
                        } else {
                            echo "<div class='sub-day'>$day</div>";
                        }
                    }
                
                ?>
            </div>
            <p>Selected day is <span class="selected"><?php echo $date_now?></span></p>
        </div>
    </div>
    <script src="script.js"></script>
</body>
</html>