<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WEB TEST</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">    
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <section id="banner">
        <nav>
            <img src="/Icon/logo.svg" alt="logo" class="logo">
            <ul>
                <li><a href="#">ABOUT</a></li>
                <li><a href="#">MENU</a></li>
                <li><a href="#">MOODS</a></li>
                <li><a href="#">BLOG</a></li>
                <li><a href="#">CONTACT</a></li>
                <li><a href="#">
                    <i class="fa fa-search"></i>
                </a></li>
            </ul>
        </nav>
        <div class="banner-text">
            <h1>Life begins after Coffee</h1>
            <p>
                <a href="#">VIEW MENU</a>
            </p>
        </div>
    </section>
    <section id="menu">
        <div class="menu-header">
            <h1>What would like to have</h1>
            <p>Coffee plunger pot sweet barista, grounds acerbic coffee instant crema cream in half and half. Spoon lungo variety, as, siphon, ristretto, iced brewed and acerbic affogato grinder.</p>
        </div>
        <div class="menu-list">
            <?php 
                $dataLayout = file_get_contents("db.json");
                $dataLayoutArr = json_decode($dataLayout, true);
                ?>
            <?php foreach ($dataLayoutArr as $data) {?>
                <div class="list-item">
                    <img src="Home/<?php echo $data["image"];?>" >
                    <div class="overlay-image"></div>
                    <p><?php echo $data["name"];?></p>
                </div>
            <?php } ?>
        </div>
        <div class="menu-footer">
            <h1>
                Extraction instant that variety white robusta strong
            </h1>
            <p>
            Coffee plunger pot sweet barista, grounds acerbic coffee instant crema cream in half and half. Spoon lungo
            variety, as, siphon, ristretto, iced brewed and acerbic affogato grinder. Mazagran café au lait wings spoon,
            percolator milk latte dark strong. Whipped, filter latte, filter aromatic grounds doppio caramelization half and half.
            </p>
        </div>
        <h1 class="btn-contact">
            <a href="#">CONTACT US</a>
        </h1>
    </section>
    <section id="health">
        <?php ?>
        <h1>Health Benefit of Coffee</h1>
        <div class="list-benefits">
            <div class="benefit-item">
                <div class="img-benefit">
                    <img src="/Icon/battery-full.svg" alt="">
                </div>
                <p>BOOST ENERGY LEVEL</p>
            </div>
            <div class="benefit-item">
                <div class="img-benefit">
                    <img src="/Icon/sun.svg" alt="">
                </div>
                <p>REDUCE DEPRESSION</p>
            </div>
            <div class="benefit-item">
                <div class="img-benefit">
                    <img src="/Icon/weight.svg" alt="">
                </div>
                <p>AID IN WEIGHT LOSS</p>
            </div>
        </div>
    </section>
    <section id="blog">
        <div class="sub-blog">
            <div class="blog-content">
                <img src="/Home/blog1.jpg" alt="">
            </div>
            <div class="blog-content">
                <div class="content-desc">
                    <p class="desc-blog">BLOG</p>
                    <p class="desc-title">Qui espresso, grounds<br>to go</p>
                    <p class="desc-date">December 12, 2019 | Espresso</p>
                    <p class="desc-desc">Skinny caffeine aged variety filter saucer redeye, sugar 
                        sit steamed eu extraction organic. Beans, crema half 
                        and half fair trade carajillo in a variety dripper doppio 
                        pumpkin spice cup lungo, doppio, est trifecta breve and, 
                        rich, extraction robusta a eu instant. Body sugar 
                        steamed, aftertaste, decaffeinated coffee fair trade sit, 
                        white shop fair trade galão, dark crema breve 
                        frappuccino iced strong siphon trifecta in a at viennese.
                        </p>
                    <p class="desc-click">
                        <a href="#">READ MORE <i class="fa fa-arrow-right"></i></a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="footer">
        <div class="footer-content">
            <div class="footer-logo">
                <img src="/Icon/logo.svg" alt="logo">
            </div>
            <div class="footer-info">
                <p>2800 S White Mountain Rd | Show Low AZ 85901</p>
                <p>(928) 537-1425 | info@grinder-coffee.com</p>
                <div class="footer-medsos">
                    <i class="fa fa-instagram"></i>
                    <i class="fa fa-facebook-square"></i>
                </div>
            </div>
            <div class="send-info">
                <p>NEWS LETTER</p>
                <form action="" class="form-info">
                    <input type="text" value="YOUR EMAIL ADDRESS" class="input-info">
                    <button type="submit" class="btn-submit-info">SUBSCRIBE</button>
                </form>
            </div>
        </div>
    </section>
</body>
</html>